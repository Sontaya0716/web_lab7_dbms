<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_Model extends CI_Model {

	public function list_all_account() {
		$result = $this->db->get('Account');

		return $result->result_array();
	}

	public function list_all_operation() {
		$result = $this->db->get('Operation');

		return $result->result_array();
	}

	public function insert_account() {
		date_default_timezone_get("Asia/Bangkok");
		$data = array(
						'ACC_No' => $this->input->post('ACC_No'),
						'ACC_Name' => $this->input->post('ACC_Name'),
						'ACC_Surname' => $this->input->post('ACC_Surname'),
						'DateOp' => date('Y-m-d h:i:s'),
						'Balance' => $this->input->post('Balance')
					);

		$this->db->insert('Account', $data);
	}

	public function deposit_account() {
		date_default_timezone_get("Asia/Bangkok");
		$data = array(
						'ACC_No_Source' => $this->input->post('ACC_No_Source'),
						'ACC_No_Dest' => NULL,
						'Action' => "D",
						'DateOp' => date('Y-m-d h:i:s'),
						'Amount' => $this->input->post('Amount')
					);

		$this->db->insert('Operation', $data);
	}

	public function withdraw_account() {
		date_default_timezone_get("Asia/Bangkok");
		$data = array(
						'ACC_No_Source' => $this->input->post('ACC_No_Source'),
						'ACC_No_Dest' => NULL,
						'Action' => "W",
						'DateOp' => date('Y-m-d h:i:s'),
						'Amount' => $this->input->post('Amount')
					);

		$this->db->insert('Operation', $data);
	}

	public function transfer_account() {
		date_default_timezone_get("Asia/Bangkok");
		$data = array(
						'ACC_No_Source' => $this->input->post('ACC_No_Source'),
						'ACC_No_Dest' => $this->input->post('ACC_No_Dest'),
						'Action' => "T",
						'DateOp' => date('Y-m-d h:i:s'),
						'Amount' => $this->input->post('Amount')
					);

		$this->db->insert('Operation', $data);
	}
}
