<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Account View</title>
</head>
<body>
	<center><h2>ตาราง Accounts</h2></center>
	<table border="1" align="center">
		<tr>
			<td colspan="5" align="center">
				<a href="<?php echo base_url(); ?>account/open_account">เปิดบัญชี</a>
				<a href="<?php echo base_url(); ?>account/deposit_account">ฝากเงิน</a>
				<a href="<?php echo base_url(); ?>account/withdraw_account">ถอนเงิน</a>
				<a href="<?php echo base_url(); ?>account/transfer_account">โอนเงิน</a>
			</td>
		</tr>
		<tr>
			<td>ACC_No</td>
			<td>ACC_name</td>
			<td>ACC_Surname</td>
			<td>DateOp</td>
			<td>Balance</td>
		</tr>

		<?php
		foreach ($account as $row) {
		?>
		<tr>
			<td><?php echo $row['ACC_No']; ?></td>
			<td><?php echo $row['ACC_Name']; ?></td>
			<td><?php echo $row['ACC_Surname']; ?></td>
			<td><?php echo $row['DateOp']; ?></td>
			<td><?php echo $row['Balance']; ?></td>
		</tr>
		<?php } ?>
	</table>
	<br>
	<center><h2>ตาราง Operations</h2></center>
	<table border="1" align="center">
		<tr>
			<th>Tran_No</th>
			<th>ACC_No_Source</th>
			<th>ACC_N0_Dest</th>
			<th>Action</th>
			<th>DateOp</th>
			<th>Amount</th>
		</tr>
		<?php
		foreach ($operation as $row) {
		?>
		<tr>
			<td><?php echo $row['Tran_No']; ?></td>
			<td><?php echo $row['ACC_No_Source']; ?></td>
			<td><?php
					if($row['ACC_No_Dest'] != NULL)
						echo $row['ACC_No_Dest'];
					else
						echo "<center>ไม่มี</center>"
				?>
			</td>
			<td><?php 
					if($row['Action'] == "D")
						echo "ฝากเงิน";
					else if($row['Action'] == "W")
						echo "ถอนเงิน";
					else if($row['Action'] == "T")
						echo "โอนเงิน";
				?>
				
			</td>
			<td><?php echo $row['DateOp']; ?></td>
			<td><?php echo $row['Amount']; ?></td>
		</tr>
		<?php } ?>
	</table>
</body>
</html>