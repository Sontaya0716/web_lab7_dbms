<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Deposit Account View</title>
</head>
<body>
	<h1 align="center">ฝากเงิน</h1>
	<form method="POST" name="open_account" action="<?php echo base_url(); ?>account/deposit_account">
		<table border="1" align="center">
			<tr>
				<td>ACC_No_Source</td>
				<td><input type="text" name="ACC_No_Source" required></td>
			</tr>

			<tr>
				<td>Amount</td>
				<td><input type="text" name="Amount" required></td>
			</tr>

			<tr>
				<td colspan="2" align="center">
					<input type="submit" name="deposit_account" value="deposit account">
					<input type="reset" name="cancel" value="cancel">
				</td>
			</tr>

		</table>
	</form>
</body>
</html>