<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Open Account View</title>
</head>
<body>
	<h1 align="center">เปิดบัญชี</h1>
	<form method="POST" name="open_account" action="<?php echo base_url(); ?>account/open_account">
		<table border="1" align="center">
			<tr>
				<td>ACC_No</td>
				<td><input type="text" name="ACC_No" required></td>
			</tr>

			<tr>
				<td>ACC_Name</td>
				<td><input type="text" name="ACC_Name" required></td>
			</tr>

			<tr>
				<td>ACC_Surname</td>
				<td><input type="text" name="ACC_Surname" required></td>
			</tr>

			<tr>
				<td>Amount</td>
				<td><input type="text" name="Balance" required></td>
			</tr>

			<tr>
				<td colspan="2" align="center">
					<input type="submit" name="insert_account" value="open account">
					<input type="reset" name="cancel" value="cancel">
				</td>
			</tr>

		</table>
	</form>
</body>
</html>