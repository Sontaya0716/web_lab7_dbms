<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	public function index()
	{
		$this->load->model('Account_Model');
		$data['account'] = $this->Account_Model->list_all_account();
		$data['operation'] = $this->Account_Model->list_all_operation();

		$this->load->view('Account_View', $data);
	}

	//function ตามlinkที่กด เปิดบัญชี
	public function open_account() {
		if($this->input->post('insert_account') != NULL) {
			$this->load->model('Account_Model');
			$this->Account_Model->insert_account();
			redirect('account');
		} else {
			$this->load->view('OpenAccount_View');
		}
	}

	// function ตามlinkที่กด ฝากเงิน
	public function deposit_account() {
		if($this->input->post('deposit_account') != NULL) {
			$this->load->model('Account_Model');
			$this->Account_Model->deposit_account();
			redirect('account');
		} else {
			$this->load->view('DepositAccount_View');
		}
	}

	// function ตามlinkที่กด ถอนเงิน
	public function withdraw_account() {
		if($this->input->post('withdraw_account') != NULL) {
			$this->load->model('Account_Model');
			$this->Account_Model->withdraw_account();
			redirect('account');
		} else {
			$this->load->view('WithdrawAccount_View');
		}
	}

	//function ตามlinkที่กด โอนเงิน
	public function transfer_account() {
		if($this->input->post('transfer_account') != NULL) {
			$this->load->model('Account_Model');
			$this->Account_Model->transfer_account();
			redirect('account');
		} else {
			$this->load->view('TransferAccount_View');
		}
	}
}
